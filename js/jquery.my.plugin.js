(function ($) {

  /*-----------------------------------------
  [Sample]
  -------------------------------------------*/
  /*
  $.radiusAct = function () {

  $("#login-form").corner('4px');

  };
  */
  /*-------------------------------------------
  [PageTopスクロールメソッド]
  ---------------------------------------------*/
  $.myPageTopAct = function () {
    var top_btn = $('#page-top');
    top_btn.click(function(){
      $('body, html').animate({scrollTop: 0}, 500);
      return false;
    });
  };
  /*-------------------------------------------
  [Bootstrapカルーセル停止再開メソッド]
  ---------------------------------------------*/
  $.myCarouselAct = function () {
    //インターバル8秒
    $("#slide-show").carousel({interval : 9000});
    //再開／停止リンクの色
    $('#slide-play').css({'color' : '#aaa'});
    //再生リンククリック時
    $('#slide-play').click(function(){
      //スライド再生
      $('#slide-show').carousel("cycle");
      $('#slide-play').css({'color' : '#aaa'});
      $('#slide-stop').css({'color' : '#fff'});
    });
    //停止リンククリック時
    $('#slide-stop').click(function(){
      //スライド停止
      $('#slide-show').carousel("pause");
      $('#slide-stop').css({'color' : '#aaa'});
      $('#slide-play').css({'color' : '#fff'});
    });
  };
  
  /*------------------------------------------
  [フォントサイズ制御jQueryメソッド]
  -------------------------------------------*/
  $.fontSizeAct = function () {
    
    var ckDays = 7;            //有効期限
    var ckPath = "/";          //cookieパス
    var defaultSize = "14px";  //bootstrapデフォルトフォントサイズ
  
    var GetCookie = function (name) {
      var arg = name + "=";
      var alen = arg.length;
      var clen = document.cookie.length;
      var i = 0;
      while(i < clen) {
        var j = i + alen;
        if(document.cookie.substring(i, j) == arg) return GetCookieVal(j);
        i = document.cookie.indexOf(" ", i) + 1;
        if(i == 0) break;
      }
      return null;
    };
    
    var GetCookieVal = function(offset) {
          var endstr = document.cookie.indexOf(";", offset);
          if(endstr == -1) endstr = document.cookie.length;
          return unescape(document.cookie.substring(offset, endstr));
    };
    
    var SetCookie = function(name, value) {
      var dobj = new Date();
      dobj.setTime(dobj.getTime() + 24 * 60 * 60 * ckDays * 1000);
      var expiresDate = dobj.toGMTString();
      document.cookie = name + '=' + escape(value) + ';expires=' + expiresDate + ';path=' + ckPath;  
    };
    
    var DeleteCookie = function(name) {
      if(GetCookie(name)) {
        document.cookie = name + '=' + '; expires=Thu, 01-Jan-70 00:00:01 GMT;path=' + ckPath;
      }
    };

    var basesize = GetCookie('saveFontSize');

    if (basesize == null) basesize = defaultSize;

    $('body').css({'cssText' : 'font-size:' + basesize + '!important'});

    $('li', '#font-sizer').click(function () {

      var cmd = this.id;

      if (cmd == "smaller") {
        var tmp = parseFloat(basesize.replace("px", ""));
        var setsize = tmp - 2;
        basesize = String(setsize) + "px";
        $('body').css({'cssText' : 'font-size:' + basesize + '!important'});
        SetCookie('saveFontSize', basesize);
      }
      else if (cmd == "larger") {
        var tmp = parseFloat(basesize.replace("px", ""));
        var setsize = tmp + 2;
        basesize = String(setsize) + "px";
        $('body').css({'cssText' : 'font-size:' + basesize + '!important'});
        SetCookie('saveFontSize', basesize);
      }
      else if (cmd == "default") {
        $('body').css({'cssText' : ''});
        basesize = defaultSize;
        DeleteCookie('saveFontSize');
      }

    });

  };


})(jQuery);